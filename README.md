<!--
SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>

SPDX-License-Identifier: EUPL-1.2
-->


<img src="src/icon/icon.png" alt="App Icon" width="64"/>

# FOX-HUD


FOX-HUD (Flight Overlay eXtended - Heads Up Display)

<img src="screenshots/preview.gif" alt="preview" width="400"/>

## Description
FOX-HUD is an overlay for WarThunder written in Qt C++.

It uses data from the WarThunder localhost:8111 API to add additional information to your Display.

Templating is done using the [inja library](https://pantor.github.io/inja/).

<img src="screenshots/screenshot1.png" alt="preview" width="400"/>

# FOX-SRV

FOX-SRV is the WebSockets based server-side implementation of FOX-HUD.

It allows players to share their data with each other.

## License
  * European Union Public Licence v1.2 (EUPL-1.2)
  * MIT (inja library)

## Project status
Currently in alpha but all core features are implemented:

  * Templating
  * Vehicle specific templates
  * General (all_air and all_tank) templates
  * Data sharing using FOX-SRV via WebSockets
  * Basic math and other functions
    * sqrt
    * pow
    * cos
    * acos
    * sin
    * asin
    * tan
    * atan
    * abs
    * PI
    * fillString
    * fillNumber
    * intToString
    * floatToString
    * floatToInt
    * subString
    * stringLength
  * Inja Functions
    * upper
    * lower
    * range
    * length
    * first
    * last
    * sort
    * join
    * round
    * odd
    * even
    * divisibleBy
    * max
    * min
    * int
    * float
    * default
    * at
    * exists
    * existsIn
    * isString
    * isArray
  * Auto-save of settings
    * Display coordinates
    * Server settings

## Building

### Install dependencies

```bash
# fedora
sudo dnf install cmake extra-cmake-modules qt6-qtbase-devel qt6-qtwebsockets-devel layer-shell-qt-devel qt6-qt5compat-devel
```

### Build
```bash
mkdir build
cd build
cmake ../src
make -j
```
