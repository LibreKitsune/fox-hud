<!--
SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>

SPDX-License-Identifier: EUPL-1.2
-->
# Template functions

## Math

### sqrt
Compute square root

#### Usage
`sqrt(float x)`

#### Example
```jinja
{{ sqrt(4) }}
```

#### Output
```
2
```

### pow
Raise to power

#### Usage
`pow(float base, float exponent)`

#### Example
```jinja
{{ pow(2, 3) }}
```

#### Output
```
8
```

### cos
Compute cosine (radians)

#### Usage
`cos(float x)`

#### Example
```jinja
{{ cos(180) }}
```

#### Output
```
-1
```

### acos
Compute arc cosine (radians)

#### Usage
`acos(float x)`

#### Example
```jinja
{{ acos(-1) }}
```

#### Output
```
3.1415...
```

### sin
Compute sine (radians)

#### Usage
`sin(float x)`

#### Example
```jinja
{{ sin(1) }}
```

#### Output
```
0.8414...
```

### asin
Compute arc sine (radians)

#### Usage
`asin(float x)`

#### Example
```jinja
{{ asin(1) }}
```

#### Output
```
1.5707...
```

### tan
Compute tangent (radians)

#### Usage
`tan(float x)`

#### Example
```jinja
{{ tan(1) }}
```

#### Output
```
1.5574...
```

### atan
Compute arc tangent (radians)

#### Usage
`atan(float x)`

#### Example
```jinja
{{ atan(1) }}
```

#### Output
```
0.7853...
```

### abs
Compute the absolute value.

#### Usage
`abs(float x)`

#### Example
```jinja
{{ abs(-1) }}
```

#### Output
```
1.0
```

### PI
Get pi.

#### Usage
`PI()`

#### Example
```jinja
{{ PI() }}
```

#### Output
```
3.1415...
```

### round
Round a number to the given precision.

#### Usage
`round(float x, int precision)`

#### Example
```jinja
{{ round(1.2345, 3) }}
```

#### Output
```
1.235
```

### odd
Check if number is odd

#### Usage
`odd(float x)`

#### Example
```jinja
{{ odd(3) }}
```

#### Output
```
true
```

### even
Check if number is even

#### Usage
`even(float x)`

#### Example
```jinja
{{ even(2) }}
```

#### Output
```
true
```

### divisibleBy
Check if number x is divisible by number y.

#### Usage
`even(float x, float y)`

#### Example
```jinja
{{ divisibleBy(4, 2) }}
```

#### Output
```
true
```

## Conversion

### int
Convert string to int.

#### Usage
`int(string nr)`

#### Example
```jinja
{{ int("1") }}
```

#### Output
```
1
```

### float
Convert string to float.

#### Usage
`float(string nr)`

#### Example
```jinja
{{ float("1.50") }}
```

#### Output
```
1.5
```

### floatToInt
Convert float to int.

#### Usage
`floatToInt(float x)`

#### Example
```jinja
{{ floatToInt(2.5) }}
```

#### Output
```
2
```

### floatToString
Convert float to String.

#### Usage
`floatToString(float x)`

#### Example
```jinja
{{ floatToString(2.5) }}
```

#### Output
```
2.5
```

### intToString
Convert float to String.

#### Usage
`intToString(int x)`

#### Example
```jinja
{{ intToString(2) }}
```

#### Output
```
2
```

## String operations
### fillString
Prepend the given character to the string to reach a defined length.

#### Usage
`fillString(string str, int length, string fillString)`

#### Example
```jinja
{{ fillString("abc", 5, "#") }}
```

#### Output
```
##abc
```

### fillNumber
Prepend the given character to the number to reach a defined length.

#### Usage
`fillNumber(int x, int length, string fillString)`

`fillNumber(float x, int length, string fillString, int precision)`

#### Example
```jinja
{{ fillNumber(3.1415, 5, "#") }}
{{ fillNumber(3.1415, 5, "#", 2) }}
```

#### Output
```
####3
#3.14
```

### subString
Extract part of a string.

#### Usage
`subString(string str, int start, int length)`

#### Example
```jinja
{{ subString("Hello world", 6, 5) }}
```

#### Output
```
world
```

### stringLength
Get length of a string.

#### Usage
`stringLength(string str)`

#### Example
```jinja
{{ stringLength("hello") }}
```

#### Output
```
5
```

### upper
Convert all letters to uppercase.

#### Usage
`upper(string str)`

#### Example
```jinja
{{ upper("hello") }}
```

#### Output
```
HELLO
```

### lower
Convert all letters to lowercase.

#### Usage
`lower(string str)`

#### Example
```jinja
{{ lower("HELLO") }}
```

#### Output
```
hello
```


## List operations
### range
Get a list of numbers.

#### Usage
`range(int x)`

#### Example
```jinja
{{ range(5) }}
```

#### Output
```
[0,1,2,3,4]
```

### length
Get the length of a list.

#### Usage
`length(list x)`

#### Example
```jinja
{{ length(["a", "b"]) }}
```

#### Output
```
2
```

### first
Get the first element of a list.

#### Usage
`first(list x)`

#### Example
```jinja
{{ first(["a", "b"]) }}
```

#### Output
```
a
```

### last
Get the last element of a list.

#### Usage
`last(list x)`

#### Example
```jinja
{{ last(["a", "b"]) }}
```

#### Output
```
b
```

### sort
Sort a list.

#### Usage
`sort(list x)`

#### Example
```jinja
{{ sort(["b", "a", "c"]) }}
```

#### Output
```
["a","b","c"]
```

### join
Join all elements of a list together.

#### Usage
`join(list x, string delimiter)`

#### Example
```jinja
{{ join(["Hello","world"], " ") }}
```

#### Output
```
Hello world
```

### max
Get the element with highest value from a list.

#### Usage
`max(list x)`

#### Example
```jinja
{{ max( [1, 2, 3] }}
```

#### Output
```
3
```

### min
Get the element with lowest value from a list.

#### Usage
`min(list x)`

#### Example
```jinja
{{ min( [1, 2, 3] }}
```

#### Output
```
1
```

### at
Get the element at the specified index (the first element is at index 0).

#### Usage
`at(list x, int index)`

#### Example
```jinja
{{ at( [1, 2, 3], 1 }}
```

#### Output
```
2
```

## Object operations
### default
Define a default value if the specified value does not exist.

#### Usage
`default(key value, var defaultValue)`

#### Example
```jinja
{{ default(wrongVariable, "Hello World") }}
```

#### Output
```
Hello World
```

### exists
Check if a key exists

#### Usage
`exists(string key)`

#### Example
```jinja
{{ exists("wrongVariableName") }}
```

#### Output
```
false
```

### existsIn
Check if a key exists in an object.

#### Usage
`existsIn(object obj, string key)`

#### Example
```jinja
{{ existsIn(state, "valid") }}
```

#### Output
```
true
```

### isString
Check if a value is a string.

#### Usage
`isString(object obj)`

#### Example
```jinja
{{ isString(1) }}
{{ isString("1") }}
```

#### Output
```
false
true
```

### isArray
Check if a value is an array/list.

#### Usage
`isArray(object obj)`

#### Example
```jinja
{{ isArray(12) }}
{{ isArray([1,2]) }}
```

#### Output
```
false
true
```