// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2
#ifndef HUDCONTROLLER_H
#define HUDCONTROLLER_H

#include <QObject>
#include "qwtapi/service/baseservice.h"
#include "qwtapi/service/indicatorsservice.h"
#include "qwtapi/service/missionservice.h"
#include "qwtapi/service/stateservice.h"

class HUDController : public QObject
{
    Q_OBJECT
public:
    explicit HUDController(QObject *parent = nullptr);
    ~HUDController();

private:
    QList<BaseService*> services;
    MissionService* missionService;
    IndicatorsService* indicatorsService;
    StateService* stateService;
    QString type;
    QString army;

signals:

};

#endif // HUDCONTROLLER_H
