// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2
#ifndef WEBSOCKETCONTROLLER_H
#define WEBSOCKETCONTROLLER_H

#include "qwtapi/controller/mapinfojsonconverter.h"
#include "qwtapi/controller/mapobjsjsonconverter.h"
#include "qwtapi/controller/missionjsonconverter.h"
#include "qwtapi/controller/statejsonconverter.h"
#include "qwtapi/controller/indicatorsjsonconverter.h"
#include "qwtapi/model/indicators.h"
#include "qwtapi/model/mapinfo.h"
#include "qwtapi/model/mapobjs.h"
#include "qwtapi/model/mission.h"
#include "qwtapi/model/state.h"
#include <QObject>
#include <QUrl>
#include <QTimer>
#include <QtWebSockets/QWebSocket>

class WebsocketController : public QObject
{
    Q_OBJECT
public:
    explicit WebsocketController(QObject *parent = nullptr);
    void establishConnection(QString userId, QString squadId, QUrl url);
    void closeConnection();
    bool isConnected();

    struct Data {
        QString userId = "";
        bool connected = true;
        State* state = nullptr;
        Indicators* indicators = nullptr;
        Mission* mission = nullptr;
        MapObjs* mapObjs = nullptr;
        Mapinfo* mapInfo = nullptr;
    };


signals:
    void stateRecieved(Data data);
    void indicatorsRecieved(Data data);
    void missionRecieved(Data data);
    void mapObjsRecieved(Data data);
    void dataRecieved(Data data);
    void mapInfoRecieved(Data data);
    void established();
    void closed();
    void pingRecieved(quint64 ping);

public slots:
    void sendState(State* state);
    void sendIndicators(Indicators* indicators);
    void sendMission(Mission* mission);
    void sendMapObjs(MapObjs* mapObjs);
    void sendMapInfo(Mapinfo* mapInfo);
    void onSslErrors(const QList<QSslError> &errors);

private slots:
    void onConnected();
    void onDisconnected();
    void onTextMessageReceived(QString message);
    void pong(quint64 ping);

private:
    QWebSocket m_webSocket;
    QUrl m_url;
    quint64 m_lastPing;
    QString m_userId;
    QString m_squadId;
    StateJsonConverter stateConverter;
    IndicatorsJsonConverter indicatorsConverter;
    MissionJsonConverter missionConverter;
    MapObjsJsonConverter mapObjsConverter;
    MapinfoJsonConverter mapInfoConverter;
    QTimer* m_pingTimer;

    QJsonObject prepareRequest();
    void sendData(QString data);

    QString m_stateKey = QStringLiteral("State");
    QString m_indicatorsKey = QStringLiteral("Indicators");
    QString m_missionKey = QStringLiteral("Mission");
    QString m_mapobjsKey = QStringLiteral("MapObjs");
    QString m_mapinfokey = QStringLiteral("Mapinfo");
    QString m_userIdKey = QStringLiteral("UserID");
    QString m_squadIdKey = QStringLiteral("SquadID");
    QString m_connectedKey = QStringLiteral("Connected");

};

#endif // WEBSOCKETCONTROLLER_H
