// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2
#include "hudcontroller.h"

HUDController::HUDController(QObject *parent)
    : QObject{parent}
{

    missionService = new MissionService();
    services.append(missionService);

    indicatorsService = new IndicatorsService();
    services.append(indicatorsService);

    stateService = new StateService();
    services.append(stateService);

    foreach(BaseService* service, services)
    {
        service->setIntervalms(100);
        service->start();
    }
}

HUDController::~HUDController()
{
    foreach(BaseService* service, services)
    {
        service->stop();
    }

    missionService = nullptr;
    indicatorsService = nullptr;
    stateService = nullptr;

    qDeleteAll(services.begin(), services.end());
    services.clear();
}
