// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2
#include "websocketcontroller.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>
#include <QJsonArray>

WebsocketController::WebsocketController(QObject *parent)
    : QObject{parent}
{
    m_pingTimer = new QTimer(this);
    m_pingTimer->setInterval(1000);

    connect(&m_webSocket, &QWebSocket::connected, this, &WebsocketController::onConnected);
    connect(&m_webSocket, &QWebSocket::disconnected, this, &WebsocketController::onDisconnected);
    connect(&m_webSocket, &QWebSocket::pong, this, &WebsocketController::pong);
    connect(m_pingTimer, &QTimer::timeout, this, [=](){ this->m_webSocket.ping();} );
    connect(&m_webSocket, &QWebSocket::textMessageReceived, this, &WebsocketController::onTextMessageReceived);
    connect(&m_webSocket, QOverload<const QList<QSslError>&>::of(&QWebSocket::sslErrors),
              this, &WebsocketController::onSslErrors);

}

void WebsocketController::establishConnection(QString userId, QString squadId, QUrl url)
{
    m_userId = userId;
    m_squadId = squadId;
    m_url = url;

    if (!m_webSocket.isValid())
    {
        m_webSocket.open(m_url);
    }
}

void WebsocketController::closeConnection()
{
    if (m_webSocket.isValid())
    {
        m_webSocket.close(QWebSocketProtocol::CloseCodeNormal, "UserRequested");
    }
}

bool WebsocketController::isConnected()
{
    return m_webSocket.isValid();
}

void WebsocketController::sendData(QString data)
{
    if (m_webSocket.isValid())
    {
       m_webSocket.sendTextMessage(data);
    }
}

void WebsocketController::sendState(State* state)
{
    QJsonObject stateJsonObj = stateConverter.toJson(state);

    QJsonObject request = prepareRequest();
    request[m_stateKey] = stateJsonObj;
    QJsonDocument json(request);

    sendData(QString::fromUtf8(json.toJson(QJsonDocument::JsonFormat::Compact)));
}

void WebsocketController::sendIndicators(Indicators* indicators)
{
    QJsonObject indicatorsJsonObj = indicatorsConverter.toJson(indicators);

    QJsonObject request = prepareRequest();
    request[m_indicatorsKey] = indicatorsJsonObj;
    QJsonDocument json(request);

    sendData(QString::fromUtf8(json.toJson(QJsonDocument::JsonFormat::Compact)));
}

void WebsocketController::sendMission(Mission* mission)
{
    QJsonObject jsonObj = missionConverter.toJson(mission);

    QJsonObject request = prepareRequest();
    request[m_missionKey] = jsonObj;
    QJsonDocument json(request);

    sendData(QString::fromUtf8(json.toJson(QJsonDocument::JsonFormat::Compact)));
}

void WebsocketController::sendMapObjs(MapObjs* mapObjs)
{
    QJsonArray jsonObj = mapObjsConverter.toJson(mapObjs);

    QJsonObject request = prepareRequest();
    request[m_mapobjsKey] = jsonObj;
    QJsonDocument json(request);

    sendData(QString::fromUtf8(json.toJson(QJsonDocument::JsonFormat::Compact)));
}

void WebsocketController::sendMapInfo(Mapinfo* mapInfo)
{
    QJsonObject jsonObj = mapInfoConverter.toJson(mapInfo);

    QJsonObject request = prepareRequest();
    request[m_mapinfokey] = jsonObj;
    QJsonDocument json(request);

    sendData(QString::fromUtf8(json.toJson(QJsonDocument::JsonFormat::Compact)));
}

void WebsocketController::onConnected()
{
    emit established();
    if (!m_pingTimer->isActive())
    {
        m_pingTimer->start();
    }
}

void WebsocketController::onTextMessageReceived(QString message)
{
    QJsonDocument json = QJsonDocument::fromJson(message.toUtf8());
    Data data;
    if (!json.isEmpty() && json.isObject())
    {
        QJsonObject jsonObj = json.object();
        QString userId = jsonObj.value(m_userIdKey).toString();
        bool connected = jsonObj.value(m_connectedKey).toBool(true);

        data.userId = userId;
        data.connected = connected;

        if (jsonObj.contains(m_stateKey))
        {
            QJsonObject stateObj = jsonObj.value(m_stateKey).toObject();
            State* state = stateConverter.stateFromJson(QJsonDocument(stateObj).toJson());
            if (state != nullptr)
            {
                data.state = state;
                emit stateRecieved(data);
            }
        }

        if (jsonObj.contains(m_indicatorsKey))
        {
            QJsonObject indicatorsObj = jsonObj.value(m_indicatorsKey).toObject();
            Indicators* indicators = indicatorsConverter.indicatorsFromJson(QJsonDocument(indicatorsObj).toJson());
            if (indicators != nullptr)
            {
                data.indicators = indicators;
                emit indicatorsRecieved(data);
            }
        }

        if (jsonObj.contains(m_missionKey))
        {
            QJsonObject missionObj = jsonObj.value(m_missionKey).toObject();
            Mission* mission = missionConverter.missionFromJson(QJsonDocument(missionObj).toJson());
            if (mission != nullptr)
            {
                data.mission = mission;
                emit missionRecieved(data);
            }
        }

        if (jsonObj.contains(m_mapobjsKey))
        {
            QJsonArray mapObjsObjs = jsonObj.value(m_mapobjsKey).toArray();
            MapObjs* mapObjs = mapObjsConverter.mapObjsFromJson(QJsonDocument(mapObjsObjs).toJson());

            if (mapObjs != nullptr)
            {
                data.mapObjs = mapObjs;
                emit mapObjsRecieved(data);
            }
        }

        if (jsonObj.contains(m_mapinfokey))
        {
            QJsonObject mapInfoObj = jsonObj.value(m_mapinfokey).toObject();
            Mapinfo* mapInfo = mapInfoConverter.mapinfoFromJson(QJsonDocument(mapInfoObj).toJson());
            if (mapInfo != nullptr)
            {
                data.mapInfo = mapInfo;
                emit mapInfoRecieved(data);
            }
        }

        emit dataRecieved(data);
    }
}

void WebsocketController::onDisconnected()
{
    emit closed();
    if (m_pingTimer->isActive())
    {
        m_pingTimer->stop();
    }
}

void WebsocketController::pong(quint64 ping)
{
    emit pingRecieved(ping);
    m_lastPing = ping;
}

QJsonObject WebsocketController::prepareRequest()
{
    QJsonObject request;
    request[m_userIdKey] = m_userId;
    request[m_squadIdKey] = m_squadId;

    return request;
}

void WebsocketController::onSslErrors(const QList<QSslError> &errors)
{
    Q_UNUSED(errors);
    m_webSocket.ignoreSslErrors();
}
