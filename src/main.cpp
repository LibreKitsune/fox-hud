// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "mainwindow.h"
#include "qwtapi/service/mapinfoservice.h"
#include "qwtapi/service/missionservice.h"
#include "qwtapi/service/indicatorsservice.h"
#include "qwtapi/service/stateservice.h"

#include <QApplication>
#include <QSettings>

#ifdef Q_OS_LINUX
#include <LayerShellQt/Shell>
#include <LayerShellQt/Window>
using namespace LayerShellQt;
#endif

int main(int argc, char *argv[])
{

    QCoreApplication::setOrganizationName("oesterle.dev");
    QCoreApplication::setOrganizationDomain("oesterle.dev");
    QCoreApplication::setApplicationName("FOX-HUD");

    #ifdef Q_OS_LINUX
        QSettings* settings = new QSettings();
        bool userLayerShell = settings->value("location/userLayershell",false).toBool();
        delete settings;
        if (!userLayerShell)
        {
            qputenv("QT_QPA_PLATFORM","xcb");
        }
        else
        {
            Shell::useLayerShell();
        }
    #endif

    QApplication a(argc, argv);

    MissionService missionService;
    IndicatorsService indicatorsService;
    StateService statesService;
    MapObjsService mapObjsService;
    MapinfoService mapInfoService;

    QList<BaseService*> services;
    services.append(&missionService);
    services.append(&indicatorsService);
    services.append(&statesService);
    services.append(&mapObjsService);
    services.append(&mapInfoService);

    foreach(BaseService* service, services)
    {
        service->setIntervalms(100);
        service->start();
    }

    MainWindow w(&missionService, &indicatorsService, &statesService, &mapObjsService, &mapInfoService);
    w.setWindowTitle(QCoreApplication::applicationName());
    w.show();

    return a.exec();
}
