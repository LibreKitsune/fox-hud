// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "injadisplay.h"
#include "ui_injadisplay.h"
#include <QMetaProperty>
#include <QStringRef>
#include <cmath>
#ifdef Q_OS_LINUX
#include <LayerShellQt/Shell>
#endif
#define _USE_MATH_DEFINES

InjaDisplay::InjaDisplay(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InjaDisplay)
{
    ui->setupUi(this);

    // Set HUD font size to 20
    QFont font = ui->lbl_value->font();
    font.setBold(true);
    font.setPointSize(20);
    ui->lbl_value->setFont(font);

    // Set HUD font colour to green
    QPalette palette = ui->lbl_value->palette();
    palette.setColor(ui->lbl_value->foregroundRole(), Qt::green);
    ui->lbl_value->setPalette(palette);

    // Enable RichText to allow for basic HTML tags
    ui->lbl_value->setTextFormat(Qt::RichText);

    // Initialize default data
    m_data["state"]["valid"] = false;
    m_data["error"] = "";

    // Initialize empty template
    m_injaTemplate = m_injaenv.parse("");

    // Add callback functions
    m_injaenv.add_callback("sqrt", 1, clb_sqrt);
    m_injaenv.add_callback("pow", 2, clb_pow);
    m_injaenv.add_callback("cos", 1, clb_cos);
    m_injaenv.add_callback("acos", 1, clb_acos);
    m_injaenv.add_callback("sin", 1, clb_sin);
    m_injaenv.add_callback("asin", 1, clb_asin);
    m_injaenv.add_callback("tan", 1, clb_tan);
    m_injaenv.add_callback("atan", 1, clb_atan);
    m_injaenv.add_callback("abs", 1, clb_abs);
    m_injaenv.add_callback("PI", 0, clb_PI);
    m_injaenv.add_callback("fillString", 3, clb_fillString);
    m_injaenv.add_callback("fillNumber", 3, clb_fillNumberInt);
    m_injaenv.add_callback("fillNumber", 4, clb_fillNumberFloat);
    m_injaenv.add_callback("intToString", 1, clb_intToString);
    m_injaenv.add_callback("floatToString", 1, clb_floatToString);
    m_injaenv.add_callback("floatToInt", 1, clb_floatToInt);
    m_injaenv.add_callback("subString", 3, clb_subString);
    m_injaenv.add_callback("stringLength", 1, clb_stringLength);

    // Update display when template changes
    connect(this, &InjaDisplay::injaTemplateChanged, this, &InjaDisplay::updateDisplay);
}

void InjaDisplay::setInjaTemplate(QString templatestr)
{
    // Validate template
    bool ok = true;
    QString errormsg = validate(templatestr, &ok);

    if (ok)
    {
        // Parse and apply template
        m_injaTemplate = m_injaenv.parse(templatestr.toStdString());
    }
    else
    {
        // Print error to template
        m_data["error"] = errormsg.toStdString();
        m_injaTemplate = m_injaenv.parse("{{ error }}");
    }

    emit injaTemplateChanged(QString::fromStdString(m_injaTemplate.content));
}

QString InjaDisplay::validate(QString templatestr, bool *ok)
{
    inja::Template injaTemplate;
    QString retval = "";
    bool valid = true;

    try
    {
        injaTemplate = m_injaenv.parse(templatestr.toStdString());
    }
    catch (inja::ParserError e) // Handle parser errors
    {
        retval = m_errortemplate
                .arg(QString::fromStdString(e.message))
                .arg(e.location.line)
                .arg(e.location.column);
        valid = false;
    }
    catch (inja::FileError e) // Handle file errors
    {
        retval = m_errortemplate
                .arg(QString::fromStdString(e.message))
                .arg(e.location.line)
                .arg(e.location.column);
        valid = false;
    }
    catch (inja::DataError e) // Handle data errors
    {
        retval = m_errortemplate
                .arg(QString::fromStdString(e.message))
                .arg(e.location.line)
                .arg(e.location.column);
        valid = false;
    }
    catch (inja::RenderError e) // Handle render errors
    {
        retval = m_errortemplate
                .arg(QString::fromStdString(e.message))
                .arg(e.location.line)
                .arg(e.location.column);
        valid = false;
    }
    catch (nlohmann::json_abi_v3_11_3::detail::parse_error e) // Handle json parse errors
    {
        retval = QString::fromStdString(e.what());
        valid = false;
    }

    if (ok != nullptr)
    {
        *ok = valid;
    }

    return retval;

}

QString InjaDisplay::injaTemplate()
{
    return QString::fromStdString(m_injaTemplate.content);
}

QMap<QString, QVariant> InjaDisplay::setData(Mission *mission, Indicators *indicators, State *state, MapObjs* mapObjs, Mapinfo* mapInfo)
{
    QVector<Data> vector;
    Data data;

    data.mission = mission;
    data.indicators = indicators;
    data.state = state;
    data.mapObjs = mapObjs;
    data.mapInfo = mapInfo;
    data.nickname = "";
    data.connected = true;

    vector.insert(0,data);

    return setData(vector, true);
}

QMap<QString, QVariant> InjaDisplay::setData(QVector<Data> data, bool onlySelf)
{
    bool update = false;

    int idx = 0;

    if (onlySelf)
    {
        idx = -1;
    }

    foreach(Data item, data)
    {
        if (item.connected)
        {
            if (item.mission != nullptr)
            {
                setMission(item.mission,idx);
                update = true;
            }

            if (item.indicators != nullptr)
            {
                setIndicators(item.indicators,idx);
                update = true;
            }

            if (item.state != nullptr)
            {
                setState(item.state,idx);
                update = true;
            }

            if (item.mapObjs != nullptr)
            {
                setMapObjs(item.mapObjs,idx);
                update = true;
            }

            if (item.mapInfo != nullptr)
            {
                setMapinfo(item.mapInfo, idx);
                update = true;
            }

            if (!item.nickname.isEmpty())
            {
                m_data["players"][idx]["name"] = item.nickname.toStdString();
            }
        }
        else
        {
            m_data["players"].erase(idx);
        }

        idx++;
    }


    if (update)
    {
        updateDisplay();
    }

    return currentDataAsQMap();
}

void InjaDisplay::setMission(Mission *mission, int idx)
{
    if (idx == -1)
    {
        m_data["mission"].clear();
    }
    else
    {
        m_data["players"][idx]["mission"].clear();
    }


    const QMetaObject *missionmetaobject = mission->metaObject();
    int missionPropertyCount = missionmetaobject->propertyCount();

    for (int propertyIndex = 0; propertyIndex < missionPropertyCount; propertyIndex++)
    {
        QMetaProperty metaproperty = missionmetaobject->property(propertyIndex);
        const char *name = metaproperty.name();

        QVariant property = mission->property(name);

        // mission.[name]
        std::string value = property.toString().toStdString();
        if (idx == -1)
        {
            m_data["mission"][name] = value;
        }
        else
        {
            m_data["players"][idx]["mission"][name] = value;
        }
    }

    const QMetaObject objectivemetaobject = Objective::staticMetaObject;
    int objectivePropertyCount = objectivemetaobject.propertyCount();

    for (int propertyIndex = 0; propertyIndex < objectivePropertyCount; propertyIndex++)
    {
        QMetaProperty metaproperty = objectivemetaobject.property(propertyIndex);
        const char *name = metaproperty.name();
        for (int objectiveIndex = 0; objectiveIndex < mission->objectives().count(); objectiveIndex++)
        {
            QVariant property = mission->objectives().at(objectiveIndex)->property(name);
            // mission.objective.[nr].[name]
            std::string value = property.toString().toStdString();
            if (idx == -1)
            {
                m_data["mission"]["objective"][objectiveIndex][name] = value;
            }
            else
            {
                m_data["players"][idx]["mission"]["objective"][objectiveIndex][name] = value;
            }
        }
    }
}

void InjaDisplay::setIndicators(Indicators *indicators, int idx)
{
    if (idx == -1)
    {
        m_data["indicator"].clear();
    }
    else
    {
        m_data["players"][idx]["indicator"].clear();
    }


    const QMetaObject *indicatorsmetaobject = indicators->metaObject();
    int indicatorsPropertyCount = indicatorsmetaobject->propertyCount();


    for (int propertyIndex = 0; propertyIndex < indicatorsPropertyCount; propertyIndex++)
    {
        QMetaProperty metaproperty = indicatorsmetaobject->property(propertyIndex);
        if (metaproperty.type() != QVariant::Type::Map) // Filter out other container types
        {
            const char *name = metaproperty.name();
            QVariant property =  indicators->property(name);
            // indicator.[name]
            std::string value = property.toString().toStdString();
            if (idx == -1)
            {
                m_data["indicator"][name] = value;
            }
            else
            {
                m_data["players"][idx]["indicator"][name] = value;
            }
        }
    }

    foreach (QString key, indicators->indicators().keys())
    {
        QVariant property = indicators->indicators().value(key);
        // indicator.[key]
        std::string value = property.toString().toStdString();
        if (idx == -1)
        {
            m_data["indicator"][key.toStdString()] = value;
        }
        else
        {
            m_data["players"][idx]["indicator"][key.toStdString()] = value;
        }
    }
}

void InjaDisplay::setState(State *state, int idx)
{
    if (idx == -1)
    {
        m_data["state"].clear();
    }
    else
    {
        m_data["players"][idx]["state"].clear();
    }


    if (state->valid())
    {
        const QMetaObject *statemetaobject = state->metaObject();
        int statePropertyCount = statemetaobject->propertyCount();

        for (int propertyIndex = 0; propertyIndex < statePropertyCount; propertyIndex++)
        {
            QMetaProperty metaproperty = statemetaobject->property(propertyIndex);
            if (metaproperty.type() != QVariant::Type::UserType) // Filter out other types (For example: engine states)
            {
                const char *name = metaproperty.name();
                QVariant property = state->property(name);

                // state.[name]
                std::string value = property.toString().toStdString();
                if (idx == -1)
                {
                    m_data["state"][name] = value;
                }
                else
                {
                    m_data["players"][idx]["state"][name] = value;
                }
            }
        }

        const QMetaObject enginemetaobject = EngineState::staticMetaObject;
        int engineStatePropertyCount = enginemetaobject.propertyCount();

        for (int propertyIndex = 0; propertyIndex < engineStatePropertyCount; propertyIndex++)
        {
            QMetaProperty metaproperty = enginemetaobject.property(propertyIndex);
            const char *name = metaproperty.name();
            for (int engineIndex = 0; engineIndex < state->engineCount(); engineIndex++)
            {
                QVariant property = state->engineStates().at(engineIndex)->property(name);

                // state.engine.[nr].[name]
                std::string value = property.toString().toStdString();
                if (idx == -1)
                {
                    m_data["state"]["engine"][engineIndex][name] = value;
                }
                else
                {
                    m_data["players"][idx]["state"]["engine"][engineIndex][name] = value;
                }
            }
        }
    }
    else
    {
        if (idx == -1)
        {
            m_data["state"]["valid"] = "false";
        }
        else
        {
            m_data["players"][idx]["state"]["valid"] = "false";
        }
    }
}

void InjaDisplay::setMapObjs(MapObjs* mapObjs, int idx)
{
    if (idx == -1)
    {
        m_data["mapObjs"].clear();
    }
    else
    {
        m_data["players"][idx]["mapObjs"].clear();
    }


    const QMetaObject metaobject = MapObj::staticMetaObject;
    int propertyCount = metaobject.propertyCount();

    for (int propertyIndex = 0; propertyIndex < propertyCount; propertyIndex++)
    {
        QMetaProperty metaproperty = metaobject.property(propertyIndex);
        const char *name = metaproperty.name();

        for(int mapObjIdx = 0; mapObjIdx < mapObjs->objs().count(); mapObjIdx++)
        {
            QVariant property = mapObjs->objs().at(mapObjIdx)->property(name);


            if (property.canConvert(QMetaType::QPointF))
            {
                QPointF point = property.toPointF();

                std::string valueX = QString::number(point.x()).toStdString();
                if (idx == -1)
                {
                    m_data["mapObjs"][mapObjIdx][name]["x"] = valueX;
                }
                else
                {
                    m_data["players"][idx]["mapObjs"][mapObjIdx][name]["x"] = valueX;
                }

                // mapObjs.[idx].[name]
                std::string valueY = QString::number(point.y()).toStdString();
                if (idx == -1)
                {
                    m_data["mapObjs"][mapObjIdx][name]["y"] = valueY;
                }
                else
                {
                    m_data["players"][idx]["mapObjs"][mapObjIdx][name]["y"] = valueY;
                }
            }
            else
            {
                // mapObjs.[idx].[name]
                std::string value = property.toString().toStdString();
                if (idx == -1)
                {
                    m_data["mapObjs"][mapObjIdx][name] = value;
                }
                else
                {
                    m_data["players"][idx]["mapObjs"][mapObjIdx][name] = value;
                }
            }
        }
    }
}

void InjaDisplay::setMapinfo(Mapinfo* mapInfo, int idx)
{
    if (idx == -1)
    {
        m_data["mapInfo"].clear();
    }
    else
    {
        m_data["players"][idx]["mapInfo"].clear();
    }



    const QMetaObject *metaobject = mapInfo->metaObject();
    int statePropertyCount = metaobject->propertyCount();

    for (int propertyIndex = 0; propertyIndex < statePropertyCount; propertyIndex++)
    {
        QMetaProperty metaproperty = metaobject->property(propertyIndex);

        const char *name = metaproperty.name();
        QVariant property = mapInfo->property(name);

        if (property.canConvert(QMetaType::QPointF))
        {
            QPointF point = property.toPointF();
            std::string valueX = QString::number(point.x()).toStdString();
            if (idx == -1)
            {
                m_data["mapInfo"][name]["x"] = valueX;
            }
            else
            {
                m_data["players"][idx]["mapInfo"][name]["x"] = valueX;
            }

            std::string valueY = QString::number(point.x()).toStdString();
            if (idx == -1)
            {
                m_data["mapInfo"][name]["y"] = valueY;
            }
            else
            {
                m_data["players"][idx]["mapInfo"][name]["y"] = valueY;
            }
        }
        else
        {
            std::string value = property.toString().toStdString();
            if (idx == -1)
            {
                m_data["mapInfo"][name] = value;
            }
            else
            {
                m_data["players"][idx]["mapInfo"][name] = value;
            }
        }
    }
}

QString InjaDisplay::currentText()
{
    return ui->lbl_value->text();
}

QString InjaDisplay::currentData()
{
    return QString::fromStdString(nlohmann::to_string(m_data));
}

QMap<QString, QVariant> InjaDisplay::currentDataAsQMap()
{
    return jsonToQmap(&m_data);
}

QMap<QString, QVariant> InjaDisplay::jsonToQmap(nlohmann::basic_json<>* json, QString key)
{
    QMap<QString, QVariant> map;

    if (json->is_object())
    {
        for (auto it = json->begin(); it != json->end(); it++)
        {
            QString keyval = QString::fromStdString(it.key());

            if (it.value().is_object())
            {
                map.insert(keyval,jsonToQmap(&it.value(),keyval));
            }
            else if (it.value().is_array())
            {
                map.insert(keyval,jsonToQmap(&it.value(),keyval));
            }
            else
            {
                map.insert(jsonToQmap(&it.value(),keyval));
            }
        }
    }

    if (json->is_array())
    {
        int nr = 0;
        for (auto it = json->begin(); it != json->end(); it++)
        {
            QString keyval = QString::number(nr); 
            map.insert(keyval,jsonToQmap(&it.value(),keyval));
            nr++;
        }
    }

    if (json->is_string())
    {
        map.insert(key,QString::fromStdString(json->get<std::string>()));
    }

    if (json->is_boolean())
    {
        map.insert(key,json->get<bool>());
    }

    if (json->is_number_integer())
    {
        map.insert(key,json->get<int>());
    }

    if (json->is_number_float())
    {
        map.insert(key,json->get<float>());
    }

    return map;
}

void InjaDisplay::updateDisplay()
{
    QString text = "";
    try
    {
        text = QString::fromStdString(m_injaenv.render(m_injaTemplate,m_data));
    }
    catch (nlohmann::json_abi_v3_11_3::detail::type_error e) // Handle json type errors
    {
        text = QString::fromStdString(e.what());
    }    
    catch (inja::ParserError e) // Handle parser errors
    {
        text = m_errortemplate
                .arg(QString::fromStdString(e.message))
                .arg(e.location.line)
                .arg(e.location.column);
    }
    catch (inja::FileError e) // Handle file errors
    {
        text = m_errortemplate
                .arg(QString::fromStdString(e.message))
                .arg(e.location.line)
                .arg(e.location.column);
    }
    catch (inja::DataError e) // Handle data errors
    {
        text = m_errortemplate
                .arg(QString::fromStdString(e.message))
                .arg(e.location.line)
                .arg(e.location.column);
    }
    catch (inja::RenderError e) // Handle render errors
    {
        text = m_errortemplate
                .arg(QString::fromStdString(e.message))
                .arg(e.location.line)
                .arg(e.location.column);
    }
    catch (nlohmann::json_abi_v3_11_3::detail::parse_error e) // Handle json parse errors
    {
        text = QString::fromStdString(e.what());
    }


    bool textChanged = text != ui->lbl_value->text();
    if (textChanged) // Only update text when a change has been detected
    {
        ui->lbl_value->setText(text);
    }
}

// START callback functions

float InjaDisplay::clb_sqrt(inja::Arguments& args)
{
    return std::sqrt(args.at(0)->get<float>());
}

float InjaDisplay::clb_pow(inja::Arguments& args)
{
    return std::pow(args.at(0)->get<float>(),args.at(1)->get<float>());
}

float InjaDisplay::clb_cos(inja::Arguments& args)
{
    return std::cos(args.at(0)->get<float>());
}

float InjaDisplay::clb_acos(inja::Arguments& args)
{
    return std::acos(args.at(0)->get<float>());
}

float InjaDisplay::clb_sin(inja::Arguments& args)
{
    return std::sin(args.at(0)->get<float>());
}

float InjaDisplay::clb_asin(inja::Arguments& args)
{
    return std::asin(args.at(0)->get<float>());
}

float InjaDisplay::clb_tan(inja::Arguments& args)
{
    return std::tan(args.at(0)->get<float>());
}

float InjaDisplay::clb_atan(inja::Arguments& args)
{
    return std::atan(args.at(0)->get<float>());
}

float InjaDisplay::clb_abs(inja::Arguments& args)
{
    return std::abs(args.at(0)->get<float>());
}

float InjaDisplay::clb_PI(inja::Arguments& args)
{
    return M_PI;
}

std::string InjaDisplay::clb_fillString(inja::Arguments& args)
{
    std::string str =args.at(0)->get<std::string>();
    int width = args.at(1)->get<int>();
    std::string fillChar = args.at(2)->get<std::string>();
    int nrToAdd = width - str.length();

    if (nrToAdd > 0)
    {
        for (int i = 0; i < nrToAdd; i++)
        {
            str = fillChar + str;
        }
    }

    return str;
}

std::string InjaDisplay::clb_fillNumberInt(inja::Arguments& args)
{
    int val = args.at(0)->get<int>();
    std::string str = std::to_string(val);
    int width = args.at(1)->get<int>();
    std::string fillChar = args.at(2)->get<std::string>();
    int nrToAdd = width - str.length();

    if (nrToAdd > 0)
    {
        for (int i = 0; i < nrToAdd; i++)
        {
            str = fillChar + str;
        }
    }

    return str;
}

std::string InjaDisplay::clb_fillNumberFloat(inja::Arguments& args)
{
    float val = args.at(0)->get<float>();
    int width = args.at(1)->get<float>();
    std::string fillChar = args.at(2)->get<std::string>();
    int precision = args.at(3)->get<float>();

    std::stringstream stream;
    stream << std::fixed << std::setprecision(precision) << val;
    std::string str = stream.str();
    int nrToAdd = width - str.length();

    if (nrToAdd > 0)
    {
        for (int i = 0; i < nrToAdd; i++)
        {
            str = fillChar + str;
        }
    }

    return str;
}

std::string InjaDisplay::clb_intToString(inja::Arguments& args)
{
    int arg = args.at(0)->get<int>();
    return QString::number(arg).toStdString();
}

std::string InjaDisplay::clb_floatToString(inja::Arguments& args)
{
    float arg = args.at(0)->get<float>();
    return QString::number(arg).toStdString();
}

int InjaDisplay::clb_floatToInt(inja::Arguments& args)
{
    return (int)args.at(0)->get<float>();
}

std::string InjaDisplay::clb_subString(inja::Arguments& args)
{
    QString str = QString::fromStdString(args.at(0)->get<std::string>());
    int pos = args.at(1)->get<int>();
    int len = args.at(2)->get<int>();
    if (0 <= pos < str.length() && 0 <= len <= str.length() - pos)
    {
        return QStringRef(&str, pos, len).toString().toStdString();
    }
    else
    {
        return str.toStdString();
    }
}

int InjaDisplay::clb_stringLength(inja::Arguments& args)
{
    return args.at(0)->get<std::string>().length();
}

// END callback functions

InjaDisplay::~InjaDisplay()
{
    delete ui;
}
