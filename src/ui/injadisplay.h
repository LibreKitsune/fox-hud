// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef INJADISPLAY_H
#define INJADISPLAY_H

#include <QWidget>
#include <QVariant>
#include "external/inja.hpp"
#include "qwtapi/model/mapinfo.h"
#include "qwtapi/model/mapobjs.h"
#include "qwtapi/model/mission.h"
#include "qwtapi/model/indicators.h"
#include "qwtapi/model/state.h"
namespace Ui {
class InjaDisplay;
}

class InjaDisplay : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QString injaTemplate MEMBER m_injaTemplate READ injaTemplate WRITE setInjaTemplate NOTIFY injaTemplateChanged);

public:
    explicit InjaDisplay(QWidget *parent = nullptr);
    ~InjaDisplay();

    struct Data {
        bool connected = true;
        QString nickname = "";
        Mission* mission = nullptr;
        Indicators* indicators = nullptr;
        State* state = nullptr;
        MapObjs* mapObjs = nullptr;
        Mapinfo* mapInfo = nullptr;
    };

public slots:
    void setInjaTemplate(QString templatestr);
    QMap<QString, QVariant> setData(Mission *mission = nullptr, Indicators *indicators = nullptr, State *state = nullptr, MapObjs* mapObjs = nullptr, Mapinfo* mapInfo = nullptr);
    QMap<QString, QVariant> setData(QVector<Data> data, bool onlySelf = false);
    void updateDisplay();

public:
    QString injaTemplate();
    QString currentText();
    QString currentData();
    QMap<QString, QVariant> currentDataAsQMap();
    QString validate(QString templatestr, bool *ok = nullptr);

signals:
    void injaTemplateChanged(QString templatestr);

private:
    Ui::InjaDisplay *ui;
    inja::Template m_injaTemplate;
    inja::Environment m_injaenv;
    inja::json m_data;

    QMap<QString, QVariant> jsonToQmap(nlohmann::basic_json<>* json, QString key = "");
    void setMission(Mission *mission, int idx = 0);
    void setIndicators(Indicators *indicators, int idx = 0);
    void setState(State *state, int idx = 0);
    void setMapObjs(MapObjs* mapObj, int idx = 0);
    void setMapinfo(Mapinfo* mapInfo, int idx = 0);

    static float clb_sqrt(inja::Arguments& args);
    static float clb_pow(inja::Arguments& args);
    static float clb_cos(inja::Arguments& args);
    static float clb_acos(inja::Arguments& args);
    static float clb_sin(inja::Arguments& args);
    static float clb_asin(inja::Arguments& args);
    static float clb_tan(inja::Arguments& args);
    static float clb_atan(inja::Arguments& args);
    static float clb_abs(inja::Arguments& args);
    static float clb_PI(inja::Arguments& args);
    static std::string clb_fillString(inja::Arguments& args);
    static std::string clb_fillNumberInt(inja::Arguments& args);
    static std::string clb_fillNumberFloat(inja::Arguments& args);
    static std::string clb_intToString(inja::Arguments& args);
    static std::string clb_floatToString(inja::Arguments& args);
    static int clb_floatToInt(inja::Arguments& args);
    static std::string clb_subString(inja::Arguments& args);
    static int clb_stringLength(inja::Arguments& args);


    QString m_errortemplate = QStringLiteral("%1 Line: %2 Column: %3");
};

#endif // INJADISPLAY_H
