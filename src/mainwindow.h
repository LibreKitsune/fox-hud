// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include "qwtapi/model/mission.h"
#include "qwtapi/model/indicators.h"
#include "qwtapi/model/state.h"
#include "qwtapi/service/mapinfoservice.h"
#include "qwtapi/service/missionservice.h"
#include "qwtapi/service/indicatorsservice.h"
#include "qwtapi/service/stateservice.h"
#include "qwtapi/service/mapobjsservice.h"
#include "templatehighlighter.h"
#include "ui/injadisplay.h"
#include "websocketcontroller.h"
#include <QLabel>
#include <QMenu>
#include <QSystemTrayIcon>
#ifdef Q_OS_LINUX
#include <LayerShellQt/Shell>
#endif
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow( MissionService* missionService, IndicatorsService* indicatorsService, StateService* statesService, MapObjsService* mapObjsService, MapinfoService* mapInfoService, QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void changeVisibilty(bool visible);
    void missionRecieved(Mission* mission);
    void indicatorsRecieved(Indicators* indicators);
    void stateRecieved(State* state);
    void mapObjsRecieved(MapObjs* state);
    void mapinfoRecieved(Mapinfo* mapInfo);
    void moveHorizontal(int x);
    void moveVertical(int y);
    void closeEvent(QCloseEvent *event) override;
    void updateUi();
    void applyTemplate();
    void validateInput();
    void saveType();
    void saveAll();
    void connectWS();
    void wsConnected();
    void wsDisconnected();
    void wsRecieved(WebsocketController::Data data);

private:
    MissionService* service;
    IndicatorsService* indicators;
    StateService* states;
    MapObjsService* mapObjs;
    MapinfoService* mapInfos;
    Ui::MainWindow *ui;
#ifdef Q_OS_LINUX
    LayerShellQt::Window* window = nullptr;
#endif
    QSystemTrayIcon *icon = nullptr;
    QMenu *m_traymenu = nullptr;
    QAction *m_action_hide;
    QAction *m_action_show;
    QAction *m_action_quit;
    InjaDisplay *m_custom_display;
    QString type;
    QString army;
    QSettings settings;
    WebsocketController* websocketController;
    QVector<QString> m_players;
    TemplateHighlighter* m_highlighter;
    QString m_basicTemplate = QStringLiteral(
                "<table>\n"
                "{# Air Example #}\n"
                "{% if exists(\"state.trueAirSpeed\") %}\n"
                "  <tr>\n"
                "    <td>TAS:</td>\n"
                "    <td>{{state.trueAirSpeed}} kph</td>\n"
                "  </tr>\n"
                "{% endif %}\n"
                "{# Tank Example #}\n"
                "{% if exists(\"indicator.first_stage_ammo\") %}\n"
                "  <tr>\n"
                "    <td>Ready Ammo:</td>\n"
                "    <td>{{indicator.first_stage_ammo}}</td>\n"
                "  </tr>\n"
                "{% endif %}\n"
                "</table>\n"
                 );

    QMap<QString, QVariant> availableValues(QVariant node, QString parrentPath = "");

    void loadType();
};
#endif // MAINWINDOW_H
