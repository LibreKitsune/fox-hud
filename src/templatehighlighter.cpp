// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2
#include "templatehighlighter.h"

TemplateHighlighter::TemplateHighlighter(QTextDocument *parent)
    : QSyntaxHighlighter(parent)
{

    Rule rule;
    m_keywordFormat.setForeground(Qt::GlobalColor::darkYellow);

    m_functionFormat.setForeground(Qt::GlobalColor::darkCyan);
    m_functionFormat.setFontItalic(true);

    m_multiLineCommentFormat.setForeground(Qt::GlobalColor::green);
    m_expressionFormat.setForeground(Qt::GlobalColor::magenta);
    m_expressionFormat.setFontWeight(QFont::Bold);
    m_statementFormat.setForeground(Qt::GlobalColor::darkCyan);
    m_statementFormat.setFontWeight(QFont::Bold);


    m_expressionStartExpression = QRegularExpression(QStringLiteral("{{"));
    m_expressionEndExpression = QRegularExpression(QStringLiteral("}}"));
    m_commentStartExpression = QRegularExpression(QStringLiteral("{#"));
    m_commentEndExpression = QRegularExpression(QStringLiteral("#}"));
    m_statementStartExpression = QRegularExpression(QStringLiteral("{%"));
    m_statementEndExpression = QRegularExpression(QStringLiteral("%}"));
    m_functionExpression = QRegularExpression(QStringLiteral("[a-zA-Z][a-zA-Z]*\\(.*\\)"));

    const QString keywordPatterns[] = {
        QStringLiteral("\\bif\\b"), QStringLiteral("\\belse\\b"), QStringLiteral("\\bendif\\b"),
        QStringLiteral("\\bfor\\b"), QStringLiteral("\\bendfor\\b"),
        QStringLiteral("\\bset\\b"), QStringLiteral("\\bin\\b"),
        QStringLiteral("\\binclude\\b")
    };


    for (const QString &pattern : keywordPatterns)
    {
        rule.pattern = QRegularExpression(pattern);
        rule.format = m_keywordFormat;
        m_rules.append(rule);
    }

    rule.pattern = m_expressionStartExpression;
    rule.format = m_expressionFormat;
    m_rules.append(rule);
    rule.pattern = m_expressionEndExpression;
    rule.format = m_expressionFormat;
    m_rules.append(rule);

    rule.pattern = m_statementStartExpression;
    rule.format = m_statementFormat;
    m_rules.append(rule);
    rule.pattern = m_statementEndExpression;
    rule.format = m_statementFormat;
    m_rules.append(rule);

    rule.pattern = m_functionExpression;
    rule.format = m_functionFormat;
    m_rules.append(rule);

}

void TemplateHighlighter::highlightBlock(const QString &text)
{
    for (const Rule &rule : qAsConst(m_rules))
    {
        QRegularExpressionMatchIterator matchIterator = rule.pattern.globalMatch(text);
        while (matchIterator.hasNext())
        {
            QRegularExpressionMatch match = matchIterator.next();
            setFormat(match.capturedStart(), match.capturedLength(), rule.format);
        }
     }

    setCurrentBlockState(0);

    int startIndex = 0;
    if (previousBlockState() != 1)
    {
        startIndex = text.indexOf(m_commentStartExpression);
    }

    while (startIndex >= 0)
    {
        QRegularExpressionMatch match = m_commentEndExpression.match(text, startIndex);
        int endIndex = match.capturedStart();
        int commentLength = 0;
        if (endIndex == -1)
        {
            setCurrentBlockState(1);
            commentLength = text.length() - startIndex;
        }
        else
        {
            commentLength = endIndex - startIndex + match.capturedLength();
        }
        setFormat(startIndex, commentLength, m_multiLineCommentFormat);
        startIndex = text.indexOf(m_commentStartExpression, startIndex + commentLength);
    }
}
