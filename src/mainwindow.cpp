// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2

#include "mainwindow.h"
#include "./ui_mainwindow.h"


#include <QWindow>
#include <QAbstractSlider>
#include <QMetaProperty>
#include <QUuid>
#include <QComboBox>
#include <QStandardPaths>
#include <QFile>
#include <QScreen>
#include <QSettings>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDir>
#include <QDesktopServices>
#ifdef Q_OS_LINUX
#include <LayerShellQt/Shell>
#endif
MainWindow::MainWindow(MissionService* missionService, IndicatorsService* indicatorsService, StateService* statesService, MapObjsService* mapObjsService, MapinfoService* mapInfoService, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);



    QScreen* desktop = QGuiApplication::primaryScreen();

    websocketController = new WebsocketController(this);
    service = missionService;
    indicators = indicatorsService;
    states = statesService;
    mapObjs = mapObjsService;
    mapInfos = mapInfoService;
    m_custom_display = new InjaDisplay();
    type = "all";
    army = "air";



    loadType();

#ifdef Q_OS_LINUX
    bool useLayerShell = settings.value("location/userLayershell",false).toBool();
    ui->cb_useLayershell->setVisible(true);
    ui->cb_useLayershell->setChecked(useLayerShell);
    if (useLayerShell)
    {
        m_custom_display->winId();
        window = LayerShellQt::Window::get(m_custom_display->windowHandle());
        window->setKeyboardInteractivity(LayerShellQt::Window::KeyboardInteractivity::KeyboardInteractivityNone);
        window->setLayer(LayerShellQt::Window::Layer::LayerOverlay);
        m_custom_display->resize(0,0);
        window->setAnchors({LayerShellQt::Window::Anchor::AnchorTop,  LayerShellQt::Window::Anchor::AnchorLeft});
        window->setExclusiveZone(0);
        window->setMargins(QMargins(200,200,0,0));
        this->setMaximumWidth(1000);
        this->setMaximumHeight(desktop->geometry().height() * 0.9);
    }
#else
    ui->cb_useLayershell->setVisible(false);
#endif

    m_traymenu = new QMenu(this);
    m_action_hide = new QAction("Hide settings",this);
    m_action_show = new QAction("Show settings",this);
    m_action_quit = new QAction("Quit",this);
    m_traymenu->addAction(m_action_show);
    m_traymenu->addAction(m_action_hide);
    m_traymenu->addAction(m_action_quit);

    QPixmap pixmap = QPixmap("://png/icon/icon.png");
    setWindowIcon(pixmap);
    icon = new QSystemTrayIcon(this);
    icon->setIcon(pixmap);
    icon->setContextMenu(m_traymenu);
    icon->show();

    m_custom_display->setAttribute(Qt::WA_TranslucentBackground, true);
    m_custom_display->setAttribute(Qt::WA_TransparentForMouseEvents, true);
    m_custom_display->setWindowFlags(
                    Qt::X11BypassWindowManagerHint |
                    Qt::FramelessWindowHint |
                    Qt::WindowStaysOnTopHint |
                    Qt::WindowTransparentForInput |
                    Qt::WindowDoesNotAcceptFocus |
                    Qt::NoDropShadowWindowHint |
                    Qt::WindowSystemMenuHint |
                    Qt::WindowMinimizeButtonHint
                    );


    ui->sb_x->setMaximum(desktop->geometry().width());
    ui->sb_y->setMaximum(desktop->geometry().height());
    m_custom_display->move(0,0);

    connect(service, &MissionService::missionRecieved, this, &MainWindow::missionRecieved);
    connect(indicators, &IndicatorsService::indicatorsRecieved, this, &MainWindow::indicatorsRecieved);
    connect(states, &StateService::stateRecieved, this, &MainWindow::stateRecieved);
    connect(mapObjs, &MapObjsService::mapObjsRecieved, this, &MainWindow::mapObjsRecieved);
    connect(mapInfos, &MapinfoService::mapinfoRecieved, this, &MainWindow::mapinfoRecieved);
    connect(ui->sb_x, QOverload<int>::of(&QSpinBox::valueChanged), this, &MainWindow::moveHorizontal);
    connect(ui->sb_y, QOverload<int>::of(&QSpinBox::valueChanged), this, &MainWindow::moveVertical);
    connect(m_action_hide, &QAction::triggered, this, &MainWindow::hide);
    connect(m_action_show, &QAction::triggered, this, &MainWindow::show);
    connect(m_action_quit, &QAction::triggered, qApp, &QApplication::quit);
    connect(ui->pb_applytemplate, &QPushButton::pressed, this, &MainWindow::applyTemplate);
    connect(ui->te_template, &QTextEdit::textChanged, this, &MainWindow::validateInput);
    connect(ui->pb_saveall, &QPushButton::pressed, this, &MainWindow::saveAll);
    connect(ui->pb_savetype, &QPushButton::pressed, this, &MainWindow::saveType);
    connect(ui->pb_connect, &QPushButton::pressed, this, &MainWindow::connectWS);
    connect(websocketController, &WebsocketController::established, this, &MainWindow::wsConnected);
    connect(websocketController, &WebsocketController::closed, this, &MainWindow::wsDisconnected);
    connect(websocketController, &WebsocketController::dataRecieved, this, &MainWindow::wsRecieved);
    connect(websocketController, &WebsocketController::pingRecieved, this, [=](quint64 ping) { this->ui->lbl_ping->setText(QString("Ping: %1").arg(ping)); });
    connect(ui->cb_useLayershell, &QCheckBox::stateChanged, this, [=]() { settings.setValue("location/userLayershell",ui->cb_useLayershell->isChecked()); });
    connect(ui->pb_openfolder, &QPushButton::clicked, this, [=]() {
        QDir dir;
        QString path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
        if (dir.mkpath(path))
        {
            QDesktopServices::openUrl(QUrl::fromLocalFile(QDir::toNativeSeparators(path)));
        }
    });


    int initial_x = settings.value("location/x",0).toInt();
    int initial_y = settings.value("location/y",0).toInt();

    if (initial_x > ui->sb_x->maximum())
    {
        initial_x = 0;
    }

    if (initial_y > ui->sb_y->maximum())
    {
        initial_y = 0;
    }

    ui->sb_x->setValue(initial_x);
    ui->sb_y->setValue(initial_y);
    ui->pb_saveall->setEnabled(false);
    ui->pb_savetype->setEnabled(false);

    ui->le_nickname->setText(settings.value("websocket/userid","").toString());
    ui->le_squadid->setText(settings.value("websocket/squadid","").toString());
    ui->le_websocketurl->setText(settings.value("websocket/server","").toString());
    ui->cb_ssl->setChecked(settings.value("websocket/ssl",false).toBool());

    QFont font = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    QFontMetrics metrics(font);

    QTextOption opt = ui->te_template->document()->defaultTextOption();
    opt.setFlags(opt.flags() | QTextOption::Flag::ShowTabsAndSpaces);
    ui->te_template->document()->setDefaultTextOption(opt);

    ui->te_template->setFont(font);
    ui->te_template->setTabStopDistance(4 * metrics.horizontalAdvance(' '));
    m_highlighter = new TemplateHighlighter(ui->te_template->document());

    changeVisibilty(true);
    applyTemplate();
    updateUi();
}

void MainWindow::changeVisibilty(bool visible)
{
    if (m_custom_display->isVisible() != visible)
    {
        if (visible)
        {
            m_custom_display->show();
        }
        else
        {
            m_custom_display->hide();
        }
    }
}

void MainWindow::missionRecieved(Mission* mission)
{
    m_custom_display->setData(mission, nullptr, nullptr);

    updateUi();
    delete mission;
}

void MainWindow::indicatorsRecieved(Indicators* indicators)
{
    websocketController->sendIndicators(indicators);

    QString recType = indicators->type().replace("tankModels/","");
    QString recArmy = indicators->indicators().value("army").toString();

    recType = recType == "" ? "all" : recType;
    recArmy = recArmy == "" ? "air" : recArmy;

    if (recType == "all")
    {
        ui->pb_savetype->setText("Save for unknown");
        ui->pb_savetype->setEnabled(false);
    }
    else
    {
        ui->pb_savetype->setText("Save for " + recType);
        ui->pb_savetype->setEnabled(true);
    }
    ui->pb_saveall->setEnabled(true);

    ui->pb_saveall->setText("Save for " + recArmy);

    if (type != recType)
    {
        type = recType;
        loadType();
        applyTemplate();
    }

    if (army != recArmy)
    {
        army = recArmy;
        loadType();
        applyTemplate();
    }

    m_custom_display->setData(nullptr, indicators, nullptr, nullptr);
    updateUi();

    delete indicators;
}

void MainWindow::stateRecieved(State* state)
{
    websocketController->sendState(state);

    m_custom_display->setData(nullptr, nullptr, state, nullptr);
    updateUi();

    delete state;
}

void MainWindow::mapObjsRecieved(MapObjs* objs)
{
    // Get only the current players position
    MapObj *obj = new MapObj();
    foreach (MapObj* mapObj, objs->objs())
    {
        if (mapObj->icon() == MapObj::Icon::PLAYER)
        {
            obj->updateWith(mapObj);
            break;
        }
    }
    MapObjs *tmpobjs = new MapObjs();
    tmpobjs->addObj(obj);

    websocketController->sendMapObjs(tmpobjs);

    m_custom_display->setData(nullptr, nullptr, nullptr, tmpobjs);
    updateUi();

    delete objs;
    delete tmpobjs;
}

void MainWindow::mapinfoRecieved(Mapinfo* mapInfo)
{
    websocketController->sendMapInfo(mapInfo);

    m_custom_display->setData(nullptr, nullptr, nullptr, nullptr, mapInfo);
    updateUi();

    delete mapInfo;
}

void MainWindow::updateUi()
{
    // Populate value preview list
    QMap<QString, QVariant> stateValues = availableValues(m_custom_display->currentDataAsQMap());
    QStringList entries;

    // Prepare list of values
    foreach(QString key, stateValues.keys())
    {
        QVariant property = stateValues.value(key);
        entries.insert(0, QString("%1: %2").arg(key).arg(property.toString()));
    }

    // Get item counts
    int itemsInWidget = ui->lw_availablevalues->count();
    int itemsInList = entries.count();

    // Update display
    int lastIndex = 0;
    for (int i = 0; i < itemsInList; i++)
    {
        QListWidgetItem* widgetItem = ui->lw_availablevalues->item(i);
        QString stringItem = entries.at(i);

        if (widgetItem != nullptr)
        {
            if (widgetItem->text() != stringItem)
            {
                widgetItem->setText(stringItem);
            }
        }
        else
        {
            ui->lw_availablevalues->addItem(stringItem);
        }
        lastIndex = i;
    }

    // Remove leftover values
    for (int i = lastIndex + 1; i < itemsInWidget; i++)
    {
        QListWidgetItem* widgetItem = ui->lw_availablevalues->takeItem(i);
        if (widgetItem != nullptr)
        {
            delete widgetItem;
        }
    }
}

// Recursive function to create list of available values from tree (Nested QMaps)
QMap<QString, QVariant> MainWindow::availableValues(QVariant node, QString parrentPath)
{
    QMap<QString, QVariant> list;
    if (node.type() == QVariant::Type::Map) // If the current node has child nodes (is a map)
    {
        // Get the true type of the node
        QMap<QString, QVariant> currentNode = node.toMap();

        // Iterate over all childern
        foreach (QString currentPath, currentNode.keys())
        {
            // Prepend the parrent path (if the node has a parrent)
            QString path = currentPath;
            if (parrentPath.size() > 0)
            {
                path = parrentPath + "." + path;
            }

            QVariant childValue = currentNode.value(currentPath);
            // recurse over all child nodes
            list.insert(availableValues(childValue,path));
        }
    }
    else // If the current node is has no child nodes (is not a map) add it to the list
    {
        list.insert(parrentPath,node.toString());
    }

    return list;
}

void MainWindow::moveHorizontal(int x)
{
    m_custom_display->move(x,m_custom_display->pos().y());
    #ifdef Q_OS_LINUX
    bool useLayerShell = settings.value("location/userLayershell",false).toBool();
    if (useLayerShell)
    {
        window->setMargins(QMargins(x,m_custom_display->pos().y(),0,0));
        m_custom_display->hide();
        m_custom_display->show();
    }
    #endif
    settings.setValue("location/x",x);
}

void MainWindow::moveVertical(int y)
{
    m_custom_display->move(m_custom_display->pos().x(), y);
    #ifdef Q_OS_LINUX
    bool useLayerShell = settings.value("location/userLayershell",false).toBool();
    if (useLayerShell)
    {
        window->setMargins(QMargins(m_custom_display->pos().x(), y,0,0));
        m_custom_display->hide();
        m_custom_display->show();
    }
    #endif
    settings.setValue("location/y",y);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    changeVisibilty(false);
}

void MainWindow::loadType()
{
    // Load vehicle specific file
    QString path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/" + type + ".txt";
    ui->le_searchpath->setText(path);
    QFile file(path);

    if (!file.exists())
    {
        // Fall back to vehicle category
        file.setFileName(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/all_" + army + ".txt");
    }

    if (file.open(QIODevice::ReadOnly))
    {
        // Load template file
        QTextStream in(&file);
        ui->te_template->setPlainText(in.readAll());
        file.close();
    }
    else
    {
        // Fall back to a basic template as a last resort
        ui->te_template->setPlainText(m_basicTemplate);
    }
}

void MainWindow::applyTemplate()
{
    m_custom_display->setInjaTemplate(ui->te_template->toPlainText());
}

void MainWindow::validateInput()
{
    bool ok = true;
    // Validate input and store error text
    QString err = m_custom_display->validate(ui->te_template->toPlainText(), &ok);

    if (ok) // if ok clear the error text label
    {
        ui->lbl_parseerror->setText("");
    }
    else // if not ok display the error text
    {
        ui->lbl_parseerror->setText(err);
    }

    // disable/enable apply button based on parser result
    ui->pb_applytemplate->setEnabled(ok);

}

void MainWindow::saveAll()
{
    QDir storageDir;
    if (storageDir.mkpath(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)))
    {
        QString path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/all_" + army + ".txt";
        QFile file(path);

        if (file.open(QIODevice::ReadWrite))
        {
            QTextStream out(&file);
            QString text = ui->te_template->toPlainText();
            out << text;
            file.close();
        }
    }
}

void MainWindow::connectWS()
{
    if (!websocketController->isConnected())
    {
        QString userId = ui->le_nickname->text();
        QString squadId = ui->le_squadid->text();
        QString server = ui->le_websocketurl->text();
        QString proto = "ws";
        if (ui->cb_ssl->isChecked())
        {
            proto = "wss";
        }
        QUrl url = QUrl(proto + "://" + server);

        websocketController->establishConnection(userId, squadId, url);

        ui->le_nickname->setEnabled(false);
        ui->le_squadid->setEnabled(false);
        ui->le_websocketurl->setEnabled(false);
        ui->pb_connect->setEnabled(false);
        ui->pb_connect->setText("Connecting...");
    }
    else
    {
        websocketController->closeConnection();
    }
}

void MainWindow::wsConnected()
{
    ui->le_nickname->setEnabled(false);
    ui->le_squadid->setEnabled(false);
    ui->le_websocketurl->setEnabled(false);
    ui->pb_connect->setEnabled(true);

    settings.setValue("websocket/userid", ui->le_nickname->text());
    settings.setValue("websocket/squadid", ui->le_squadid->text());
    settings.setValue("websocket/server", ui->le_websocketurl->text());
    settings.setValue("websocket/ssl", ui->cb_ssl->isChecked());

    ui->pb_connect->setText("Disconnect");
}

void MainWindow::wsDisconnected()
{
    ui->le_nickname->setEnabled(true);
    ui->le_squadid->setEnabled(true);
    ui->le_websocketurl->setEnabled(true);
    ui->pb_connect->setEnabled(true);
    ui->pb_connect->setText("Connect");
    ui->lbl_ping->setText("");

    // Clear player data by passing empty data structs and connected set to "false".
    // This will instruct the inja display to delete the data
    QVector<InjaDisplay::Data> playerData;
    for (int i = 0; i < m_players.size(); i++)
    {
        InjaDisplay::Data curPlayerData;
        curPlayerData.connected = false;
        curPlayerData.nickname = m_players.at(i);
        playerData.insert(i,curPlayerData);
    }
    m_custom_display->setData(playerData);
    m_players.clear();
}

void MainWindow::wsRecieved(WebsocketController::Data data)
{
    if (data.connected && !m_players.contains(data.userId))
    {
        m_players.append(data.userId);
    }

    int idx = m_players.indexOf(data.userId);

    QVector<InjaDisplay::Data> playerData;
    for (int i = 0; i < m_players.size(); i++)
    {
        InjaDisplay::Data curPlayerData;
        if (i == idx)
        {
            curPlayerData.connected = data.connected;
            curPlayerData.indicators = data.indicators;
            curPlayerData.state = data.state;
            curPlayerData.mission = data.mission;
            curPlayerData.mapObjs = data.mapObjs;
            curPlayerData.mapInfo = data.mapInfo;
        }
        curPlayerData.nickname = m_players.at(i);
        playerData.insert(i, curPlayerData);
    }

    if (!data.connected)
    {
        m_players.removeAll(data.userId);
    }

    m_custom_display->setData(playerData);
    updateUi();

    if (data.state != nullptr)
    {
        delete data.state;
    }

    if (data.indicators != nullptr)
    {
        delete data.indicators;
    }

    if (data.mission != nullptr)
    {
        delete data.mission;
    }

    if (data.mapObjs != nullptr)
    {
        delete data.mapObjs;
    }

    if (data.mapInfo != nullptr)
    {
        delete data.mapInfo;
    }
}

void MainWindow::saveType()
{
    QDir storageDir;
    if (storageDir.mkpath(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)))
    {
        QString path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/" + type + ".txt";
        QFile file(path);

        if (file.open(QIODevice::ReadWrite))
        {
            QTextStream out(&file);
            QString text = ui->te_template->toPlainText();
            out << text;
            file.close();
        }
    }
}

MainWindow::~MainWindow()
{
    m_traymenu->close();
    websocketController->closeConnection();
    delete m_custom_display;
    delete ui;
}


