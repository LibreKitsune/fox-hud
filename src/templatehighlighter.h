// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2
#ifndef TEMPLATEHIGHLIGHTER_H
#define TEMPLATEHIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QRegularExpression>
#include <QTextCharFormat>

class TemplateHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT
public:
    TemplateHighlighter(QTextDocument *parent = nullptr);

protected:
    void highlightBlock(const QString &text) override;

private:
    struct Rule
    {
        QRegularExpression pattern;
        QTextCharFormat format;
    };

    QVector<Rule> m_rules;
    QTextCharFormat m_keywordFormat;
    QTextCharFormat m_multiLineCommentFormat;
    QTextCharFormat m_expressionFormat;
    QTextCharFormat m_statementFormat;
    QTextCharFormat m_functionFormat;

    QRegularExpression m_expressionStartExpression;
    QRegularExpression m_expressionEndExpression;
    QRegularExpression m_commentStartExpression;
    QRegularExpression m_commentEndExpression;
    QRegularExpression m_statementStartExpression;
    QRegularExpression m_statementEndExpression;
    QRegularExpression m_functionExpression;


};

#endif // TEMPLATEHIGHLIGHTER_H
