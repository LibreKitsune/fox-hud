<!--
SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>

SPDX-License-Identifier: EUPL-1.2
-->

# FOX-SRV

FOX-SRV is the WebSockets based server-side implementation of FOX-HUD.

It allows players to share their data with each other.

## Building

### Install dependencies

```bash
# fedora
sudo dnf install cmake extra-cmake-modules qt6-qtbase-devel qt6-qtwebsockets-devel
```

### Build
```bash
mkdir build
cd build
cmake ../
make -j
```
