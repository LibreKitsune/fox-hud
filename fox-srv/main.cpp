// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2
#include "foxsrvserver.h"
#include <QCoreApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QCommandLineParser parser;
    parser.setApplicationDescription("FOX-SRV: WebSocket server for FOX-HUD");
    parser.addHelpOption();

    QCommandLineOption portOption({"p","port"}, "Listen on Port","port","1234");
    parser.addOption(portOption);

    QCommandLineOption secureOption({"s","secure"}, "Use SSL");
    parser.addOption(secureOption);

    QCommandLineOption keyFile({"k","key"}, "Key File Path","keyfile","fox-srv.key");
    parser.addOption(keyFile);

    QCommandLineOption certFile({"c","cert"}, "Cert File Path","certfile","fox-srv.cert");
    parser.addOption(certFile);

    parser.process(a);

    int port = parser.value(portOption).toInt();
    bool secure = parser.isSet(secureOption);
    QString cert = parser.value(certFile);
    QString key = parser.value(keyFile);

    FoxSrvServer* server = new FoxSrvServer(port, secure, cert, key, nullptr);

    if (!server->listen())
    {
        return 1;
    }

    return a.exec();
}
