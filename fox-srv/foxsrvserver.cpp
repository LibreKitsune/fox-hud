// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2
#include "foxsrvserver.h"
#include <QCoreApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QSslKey>

FoxSrvServer::FoxSrvServer(quint16 port, bool secure, QString certFile, QString keyFile, QObject *parent)
    : QObject{parent},

      m_port(port)
{    


    if (secure)
    {
        m_pWebSocketServer = new QWebSocketServer(QStringLiteral("FOX-SRV"), QWebSocketServer::SecureMode, this);
        QSslConfiguration sslConfig;
        QFile cert(certFile);
        QFile key(keyFile);

        if (cert.open(QIODevice::ReadOnly) && key.open(QIODevice::ReadOnly))
        {
            QSslCertificate certificate(&cert, QSsl::Pem);
            QSslKey sslKey(&key, QSsl::Rsa, QSsl::Pem);

            sslConfig.setPeerVerifyMode(QSslSocket::VerifyNone);
            sslConfig.setLocalCertificate(certificate);
            sslConfig.setPrivateKey(sslKey);
            m_pWebSocketServer->setSslConfiguration(sslConfig);
            qInfo() << logDate() << "Loaded SSL Config" << certFile << keyFile;
        }
        else
        {
            m_sslSuccessfull = false;
            qWarning() << logDate() << "Could not load" << certFile << keyFile;
        }
    }
    else
    {
        m_pWebSocketServer = new QWebSocketServer(QStringLiteral("FOX-SRV"), QWebSocketServer::NonSecureMode, this);
    }
}

FoxSrvServer::~FoxSrvServer()
{
    m_pWebSocketServer->close();
    qDeleteAll(m_clients.begin(), m_clients.end());
}

bool FoxSrvServer::listen()
{
    bool success = false;
    if (m_sslSuccessfull)
    {
        success = m_pWebSocketServer->listen(QHostAddress::Any, m_port);

        if (success)
        {
            qInfo() << logDate() << m_pWebSocketServer->serverName() << "listening on port" << m_port;
            connect(m_pWebSocketServer, &QWebSocketServer::newConnection, this, &FoxSrvServer::onNewConnection);
        }
    }

    return success;
}

void FoxSrvServer::onNewConnection()
{
    QWebSocket *wssender = m_pWebSocketServer->nextPendingConnection();

    connect(wssender, &QWebSocket::textMessageReceived, this, &FoxSrvServer::processTextMessage);
    connect(wssender, &QWebSocket::disconnected, this, &FoxSrvServer::socketDisconnected);

    qInfo() << logDate() << "New Connection:" << wssender->peerAddress().toString() << wssender->peerPort();
    m_clients << wssender;
}


void FoxSrvServer::processTextMessage(QString message)
{
    QWebSocket *wssender = qobject_cast<QWebSocket *>(sender());

    QJsonParseError parseError;
    QJsonDocument json = QJsonDocument::fromJson(message.toUtf8(), &parseError);

    if (wssender && json.isObject())
    {
        QJsonObject jsonObj = json.object();
        QString squadId = "";
        QString userId = "";
        // Validate QString
        if (jsonObj.contains(m_squadIdKey) && jsonObj.contains(m_userIdKey))
        {
            squadId = jsonObj.value(m_squadIdKey).toString();
            userId = jsonObj.value(m_userIdKey).toString();
        }

        if (!squadId.isEmpty() && !userId.isEmpty())
        {

            jsonObj[m_connectedKey] = true;

            // Map websocket to userid
            if (!m_WSUserIdMap.contains(wssender))
            {
                m_WSUserIdMap.insert(wssender, userId);
            }

            // Create new squad
            if (!m_squads.contains(squadId))
            {
                qInfo() << logDate() << "Create squad" << squadId;
                QList<QWebSocket*>* newClientList = new QList<QWebSocket*>;
                newClientList->append(wssender);
                m_squads.insert(squadId,newClientList);
            }

            // Get client list for squadId
            QList<QWebSocket*>* clientsToSendTo = m_squads.value(squadId);

            // Add player to squad
            if (!clientsToSendTo->contains(wssender))
            {
                qInfo() << logDate() << "Add player to squad" << squadId << userId << wssender->peerAddress().toString();
                clientsToSendTo->append(wssender);
            }

            // Send data to all members of the squad
            foreach(QWebSocket* wsclient, *clientsToSendTo)
            {
                // Dont send data to the sender
                if (wsclient != wssender)
                {
                    wsclient->sendTextMessage(QJsonDocument(jsonObj).toJson(QJsonDocument::Compact));
                }
            }
        }
    }
    else
    {
        if (wssender)
        {
            qWarning() << logDate() << wssender->peerAddress().toString() << wssender->peerPort() << parseError.errorString();
        }
    }
}

void FoxSrvServer::socketDisconnected()
{
    QWebSocket *wssender = qobject_cast<QWebSocket *>(sender());

    if (wssender)
    {
        m_clients.removeAll(wssender);

        foreach(QString key, m_squads.keys())
        {
            QList<QWebSocket*>* list = m_squads.value(key);
            if (list->contains(wssender))
            {
                QString userId = m_WSUserIdMap.value(wssender, "");
                list->removeAll(wssender);
                qInfo() << logDate() << "remove from m_squads" << key << userId << wssender->peerAddress().toString();


                m_WSUserIdMap.remove(wssender);

                if (!userId.isEmpty())
                {
                    QJsonObject jsonObj;
                    jsonObj[m_userIdKey] = userId;
                    jsonObj[m_connectedKey] = false;
                    jsonObj[m_squadIdKey] = "";

                    // Send disconnect message to all members of the squad
                    foreach(QWebSocket* wsclient, *list)
                    {
                        if (wssender != wsclient)
                        {
                            wsclient->sendTextMessage(QJsonDocument(jsonObj).toJson(QJsonDocument::Compact));
                        }
                    }
                }

                if (list->isEmpty())
                {
                    qInfo() << logDate() << "squad is empty" << key;
                    m_squads.remove(key);
                    delete list;
                    list = nullptr;
                }
                break;
            }
        }
        qInfo() << logDate() << "Disconnected:" << wssender->peerAddress().toString() << wssender->peerPort();

        wssender->deleteLater();
    }
}

QString FoxSrvServer::logDate()
{
    QDateTime current = QDateTime::currentDateTime();
    return current.toString("dd.MM.yyyy hh:mm:ss");
}
