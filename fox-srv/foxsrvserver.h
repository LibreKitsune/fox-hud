// SPDX-FileCopyrightText: 2024 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: EUPL-1.2
#ifndef FOXSRVSERVER_H
#define FOXSRVSERVER_H

#include <QObject>
#include <QWebSocket>
#include <QWebSocketServer>

class FoxSrvServer : public QObject
{
    Q_OBJECT
public:
    explicit FoxSrvServer(quint16 port, bool secure = false, QString certFile = "fox-srv.cert", QString keyFile = "fox-srv.key", QObject *parent = nullptr);
    ~FoxSrvServer();
    bool listen();

signals:
    void closed();

private slots:
    void onNewConnection();
    void processTextMessage(QString message);
    void socketDisconnected();

private:
    QString logDate();

    QWebSocketServer *m_pWebSocketServer;
    QList<QWebSocket*> m_clients;
    QMap<QWebSocket*,QString> m_WSUserIdMap;
    QMap<QString,QList<QWebSocket*>*> m_squads;
    quint16 m_port;
    bool m_sslSuccessfull = true;

    QString m_userIdKey = QStringLiteral("UserID");
    QString m_squadIdKey = QStringLiteral("SquadID");
    QString m_connectedKey = QStringLiteral("Connected");

};

#endif // FOXSRVSERVER_H
